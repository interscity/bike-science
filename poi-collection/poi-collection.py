from flask import Flask, render_template, request, Response
import os
import atexit
import json

app = Flask(__name__)
output_file = open('data_file', 'a')

def close_file():
    output_file.close()
    print('Output file was closed.')

atexit.register(close_file)


@app.route('/')
def hello():
    start = request.args.get('start', '')
    if start == '':
        return "Please fill in the 'start' parameter at URL with the cell index"
    return render_template('index.html', start=start)
    
@app.route('/grid-bounds.js')
def grid_bounds():
    return Response(render_template('grid-bounds.js'), mimetype='application/javascript')
    
@app.route('/save', methods=['POST'])
def save():
    try:
        chunk = json.loads(request.form['json'])
        for poi in chunk:
            poi_str = json.dumps(poi)
            output_file.write(poi_str)
            output_file.write('\n')
        return '', 204
    except Exception as e:
        print('------------------------------------')
        print('ERROR!')
        print(request.form['json'])
        raise e
        
if __name__ == '__main__':
    app.run()
