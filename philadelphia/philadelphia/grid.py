import sys
sys.path.append("..")
from bikescience import grid

PHIL_LAT = 39.95233
PHIL_LON = -75.16379

def create(n=14, west_offset=-.065, east_offset=.04, north_offset=.042, south_offset=-.045,
                 h_displacement=0, v_displacement=0):
    return grid.Grid(n, 
                     west_limit=PHIL_LON + west_offset + h_displacement,
                     east_limit=PHIL_LON + east_offset + h_displacement,
                     north_limit=PHIL_LAT + north_offset + v_displacement,
                     south_limit=PHIL_LAT + south_offset + v_displacement)     